#!/usr/bin/python3

# Class maken met een functie die initieert als de class word geladen.
class MyClass:
 a = 0
 b = 0
 c = 0
 def __init__(self):
  print('Hello World')
 def getdata(self, x, y):
  self.a = x
  self.b = y
 def addnumbers(self):
  self.c = self.a + self.b
 def print_sum(self):
  print('sum = %d' % self.c)

class MyNewClass(MyClass):
 def __init__(self):
  print('Hello New Class')
 def print_message(self):
  print('My New Class')

#mc = MyClass()
mc = MyNewClass()
mc.getdata(3,4)
mc.addnumbers()
mc.print_sum()
