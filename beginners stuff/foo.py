#!/usr/bin/python3

naam1 = 'Jeffrey de Boer'
naam2 = 'Lotte Kalsbeek'

num1 = 3
num2 = 6

print(naam1 + ' ' + naam2)
print(naam1 ,  naam2,sep=' <--> ')
print(naam1, num1, naam2, num2, sep=' <--> ')

print('Namen zijn %s en %s' % (naam1,naam2))
print('Nummmers zijn %s en %s' % (num1,num2))
print('Met andere format... nummers zijn {0} en {1}' .format(num1,num2))

print('Nummmers zijn %5s en %5s' % (3,25))
print('Nummmers zijn %5s en %5s' % (56,534))

print('Naam1: %20s' % naam1)
print('Naam2: %20s' % naam2)
