#!/usr/bin/python3

import threading
import os, multiprocessing
from time import sleep

sema1 = multiprocessing.Semaphore(0)
sema2 = multiprocessing.Semaphore(0)

class Threads (threading.Thread):
	while True:
	        def functie1 (args):
        	        sema1.acquire()
	                time.sleep(2)
	                sema2.acquire()
	                sema2.release()
	                sema1.release()
	                print('Thread 1 zegt: ik ben done')

	        def functie2 (args):
	                sema2.acquire()
	                time.sleep(1)
	                sema1.acquire()
	                sema1.release()
	                sema2.release()
	                print('Thread 2 zegt: ik ben ook done')

#go = Threads()
#go.threaded_functie1()
#go.threaded_functie2()

t1 = Threads()
t2 = Threads()

t1.start()
t2.start()

print('all done')
