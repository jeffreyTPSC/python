#!/usr/bin/python3

text = '''
I see you gathered before me. Hungry, terrified, clutching your babes to your breast. Emperor Emhyr has marched his legions into our lands, laid siege to every fortress from here to the Blue Mountains. Rabid and ravenous he bites and bites away. Man of the North, you stand at the precipice! Your kings have failed you, so now you turn to the Gods. And yet you do not plead. You do not kneel to dust your heads with ash. Instead you wail why have the Gods forsaken us. We must look into the trials we've failed long ago. In a time past our world intertwined with another through an upheaval scholars call the Conjunction of the Spheres. The Gods allowed unholy forces to slip into our domain. The offspring of that cataclysm was the nefarious force called Magic.
''' 

# Pak een string en maak een freq. tabel. For loop telt de occurence van een string value.
# als die niet voor komt (nog niet) dan krijgt ie in de ftble:
def freq_table(txt):
    ftble = {}
    for c in txt:
        if c not in ftble:
            ftble[c] = 1
        else:
            ftble[c] += 1
    return ftble

### this is a comparator function that allows us to compare
### Huffman nodes to each other.
def huff_node_freq_cmp(node1, node2):
    freq1, freq2 = get_freq(node1), get_freq(node2)
    if freq1 < freq2:
        return -1
    elif freq1 > freq2:
        return 1
    else:
        return 0

### insert an item into its appropriate place in a list given
### a comparator function cmpf, e.g., huff_node_freq_cmp
def insert_item(item, lst, cmpf):
    def insert_item_aux(item, lst, cmpf, n, pos):
        if pos == n:
            lst.append(item)
        elif cmpf(item, lst[pos]) == -1 or cmpf(item, lst[pos]) == 0:
            lst.insert(pos, item)
        else:
            insert_item_aux(item, lst, cmpf, n, pos+1)
        
    insert_item_aux(item, lst, cmpf, len(lst), 0)

### one way to combine two Huffman nodes is to let the left node
### be the left branch and the right node be the right branch.
def combine_left_right(n1, n2):
    return make_huffman_tree(n1, n2)

### another way to combine two Huffman nodes is to let the left node
### be the right branch and the right node be the left branch.
def combine_right_left(n1, n2):
    return make_huffman_tree(n2, n1)

### the work horse function called by build_huffman_tree below.
### It takes a list of Huffman nodes and a combiner function, e.g.,
### combine_left_right and returns a Huffman tree.
### Important: nodes are assumed to be sorted by their
### frequencies from smallest to largest. So that the first
### two nodes in nodes
def build_huffman_tree_aux(node_lst, combine):
    if len(node_lst) == 0:
        return None
    elif len(node_lst) == 1:
        return node_lst[0]
    else:
        ## keep looping so long as there are more than two nodes
        ## in node_lst. 
        while len(node_lst) > 2:
            ## 1. combine two nodes with the lowest frequencies
            new_node = combine(node_lst[0], node_lst[1])
            ## 2. delete two combined nodes from node_lst
            del node_lst[0]
            del node_lst[0]
            ## 3. insert new_node into node_lst into its right
            ## position.
            insert_item(new_node, node_lst, huff_node_freq_cmp)
            ## print nodes
        else:
            return make_huffman_tree(node_lst[0], node_lst[1])

def build_huffman_tree(txt, combine=combine_left_right):
    ### 1. build a frequency table from txt
    ftble = freq_table(txt)
    ### 2. get the list of Huffman nodes
    node_lst = ftble.items()

    ### 3. sort node_lst by the node frequency from smallest
    ###    to largest
    node_lst.sort(huff_node_freq_cmp)

    ### 4. call the above build_huffman_tree_aux workhorse
    return build_huffman_tree_aux(node_lst, combine)
