#!/usr/bin/python3
from random import randint					# Import randint van random.


class Node(object):						# Class om object list aan te maken.
	def __init__(self, d, n = None):			# Functie voor initialisatie.
		self.data = d
		self.next_node = n

	def get_next (self):					# Functie om de "pointer" op te halen.
		return self.next_node

	def set_next (self, n):					# Functie om de "pointer" te verplaatsen.
		self.next_node = n

	def get_data (self):					# Functie om data op te halen.
		return self.data

	def set_data (self, d):					# Functie om data in d var. te zetten.
		self.data = d

class LinkedList (object):					# Maak objectlist aan.
	def __init__(self, r = None):
		self.root = r
		self.size = 0

	def get_size (self):					# Bepaal de groote van de list.
		return self.size

	def add (self, d):					# Maak een nieuwe "node" (/object) aan.
		new_node = Node (d, self.root)
		self.root = new_node
		self.size += 1

	def remove (self, d):					# Verwijder node met while loop voor err. detectie.
		this_node = self.root
		prev_node = None

		while this_node:
			if this_node.get_data() == d:
				if prev_node:
					prev_node.set_next(this_node.get_next())
				else:
					self.root = this_node.get_next()
				self.size -= 1
				return True	#remove data
			else:
				prev_node = this_node
				this_node = this_node.get_next()
		return False 			#data niet gevonden

	def find (self, d):					# Zoek functie om data in de object te zoeken.
		this_node = self.root
		while this_node:
			if this_node.get_data() == d:
				return d
			else:
				self.root = this_node.get_next()
		return None
		
	def printlist (self):					# Functie om object list uit te printen.
		node = self.root
		#node.sort(key=lambda x: x.count, reverse=True)	# Sort met lambda key, werkt niet.
		#node.sort()
		
		#except AttributeError:
		#	pass

		while node != None:
			print (node.data)
			node = node.next_node

		#node = sorted(node, key=lambda node: node.data)

# Maak lists aan en geef ze een random value tussen de 0 en de 10. en print het zooitje uit.

myList = LinkedList()
print('LinkedList nummers !')
myList.add(randint(0,10))
myList.add(randint(0,10))
myList.add(randint(0,10))
myList.printlist()

# Cheat sheet.

# Usage class
#myList.remove(8)
#print(myList.remove(12))
#print(myList.find(5))
#print(myList.find(3))
#print(myList.data)

# Usage randint (uit random)
#print('Print even random nummertje')
#print (randint(0,20))
#print('Done')

